package com.shymoniak.view;

import com.shymoniak.model.constants.ConsoleDefaultMessage;
import com.shymoniak.controller.Controller;

import java.util.Scanner;

/**
 * The class <p>UserInteraction</p> provides console interface for this program
 * User chooses operations from list printed in console and class <p>Controller</p>
 * performs them.
 */
public class UserInteraction {
    //Constant String value that is used if user enters wrong number
    public static final String INVALID_NUMBER = "YOU ENTERED INVALID NUMBER, PLEASE TRY AGAIN";

    Controller controller = new Controller();
    ConsoleDefaultMessage consoleDefaultMessages = new ConsoleDefaultMessage();

    Scanner scan = new Scanner(System.in);

    /**
     * Temporary variables which are used to save information entered in console
     */
    int fromUser;
    int from;
    int to;
    String tempStr;

    /**
     * Method which
     */
    public void consoleInterface() {
        consoleDefaultMessages.printMenu();
        fromUser = scan.nextInt();
        if (fromUser == 1) {
            controller.printRealEstates();
        } else if (fromUser == 2) {
            printByType();
        } else if (fromUser == 3) {
            findByParameter();
        } else if (fromUser == 4) {
            editRealEstate();
        } else if (fromUser == 9) {
            return;
        } else {
            System.err.println(INVALID_NUMBER);
        }
        consoleInterface();
    }

    /**
     * Prints possible types of real estate in console, gets printed number by user
     * from console and calls methods from <p>Controller</p> based on users choice.
     * If user enters 1 - prints available flats in console
     * If user enters 2 - prints available mansions in console
     * If user enters 3 - prints available penthouses in console
     * If user enters any other number - prints error
     */
    private void printByType() {
        consoleDefaultMessages.printRealEstateTypes();
        fromUser = scan.nextInt();
        if (fromUser == 1) {
            controller.printFlats();
        } else if (fromUser == 2) {
            controller.printMansions();
        } else if (fromUser == 3) {
            controller.printPenthouses();
        } else {
            System.err.println(INVALID_NUMBER);
        }
    }

    /**
     * Prints possible searching parameters of real estate in console, gets number printed
     * by user in console and calls methods from <p>Controller</p> based on users choice.
     * If user enters 1 - prints sorted list by price of real estates found within entered bounds
     * If user enters 2 - prints sorted list of real estates found by city name
     * If user enters 3 - prints sorted list by area of real estates found within entered bounds
     * If user enters any other number - prints error
     */
    private void findByParameter() {
        consoleDefaultMessages.printSearchParameters();
        fromUser = scan.nextInt();
        if (fromUser == 1) {
            System.out.println("Enter the lowest and the highest price for real estate");
            from = scan.nextInt();
            to = scan.nextInt();
            controller.searchByPrice(from, to);
        } else if (fromUser == 2) {
            System.out.println("Enter the name of the city\n" +
                    "available cities: 'Lviv', 'Kyiv', 'Odessa'");
            tempStr = scan.next();
            if (tempStr.equals("Lviv") || tempStr.equals("Kyiv") || tempStr.equals("Odessa")) {
                controller.searchByCity(tempStr);
            } else {
                System.err.println("YOU ENTERED WRONG CITY, PLEASE TRY AGAIN");
            }
        } else if (fromUser == 3) {
            System.out.println("Enter the smallest and the biggest area for real estate");
            from = scan.nextInt();
            to = scan.nextInt();
            controller.searchByArea(from, to);
        } else {
            System.err.println(INVALID_NUMBER);
        }
    }

    /**
     * Gives user an opportunity to add, modify and delete real estates
     * If user enters 1 - user enters new real estate which is then
     * added in "Data Base"
     * If user enters 2 - user enters id of real estate which he wants
     * to modify, and then enters new real estate
     * If user enters 3 - user enters id of real estate which he wants
     * to delete
     * If user enters any other number - prints error
     */
    private void editRealEstate() {
        consoleDefaultMessages.printRealEstateEditFunctions();
        fromUser = scan.nextInt();
        if (fromUser == 1) {
            consoleDefaultMessages.printRealEstateTypes();
            if (fromUser == 1) {
                controller.addFlat();
            } else if (fromUser == 2) {
                controller.addMansion();
            } else if (fromUser == 3) {
                controller.addPenthouse();
            } else {
                System.err.println(INVALID_NUMBER);
            }
        } else if (fromUser == 2) {
            System.out.println("Enter id of real estate you want to modify");
            fromUser = scan.nextInt();
            System.out.println("Enter new real estate");
            consoleDefaultMessages.printRealEstateTypes();
            if (fromUser == 1) {
                controller.modifyFlat(fromUser);
            } else if (fromUser == 2) {
                controller.modifyMansion(fromUser);
            } else if (fromUser == 3) {
                controller.modifyPenthouse(fromUser);
            } else {
                System.err.println(INVALID_NUMBER);
            }
        } else if (fromUser == 3) {
            System.out.println("Enter id of real estate you want to delete");
            fromUser = scan.nextInt();
            controller.deleteById(fromUser);
        } else {
            System.err.println(INVALID_NUMBER);
        }
    }

}
