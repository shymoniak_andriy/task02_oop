package com.shymoniak.model.realestates;

public class Flat extends RealEstate {
    private int onFloor;

    public Flat() {
    }

    public Flat(int area, int price, int rooms, int distToSchool, int distToHospital,
                int distToAirport, String city, int onFloor) {
        super(area, price, rooms, distToSchool, distToHospital, distToAirport, city);
        this.onFloor = onFloor;
    }

    public int getOnFloor() {
        return onFloor;
    }

    public void setOnFloor(int onFloor) {
        this.onFloor = onFloor;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "id = " + getId() +
                ", area = " + getArea() +
                ", price = " + getPrice() +
                ", rooms = " + getRooms() +
                ", distance to school = " + getDistToSchool() +
                ", distance to hospital = " + getDistToHospital() +
                ", distance to airport = " + getDistToAirport() +
                ", city = '" + getCity() +'\'' +
                ", on floor = " + onFloor +
                '}';
    }
}
