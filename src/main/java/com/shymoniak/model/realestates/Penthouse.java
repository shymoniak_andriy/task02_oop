package com.shymoniak.model.realestates;

import com.shymoniak.model.realestates.Flat;

public class Penthouse extends Flat {
    private boolean hasPool;

    public Penthouse() {
    }

    public Penthouse(int area, int price, int rooms, int distToSchool, int distToHospital,
                     int distToAirport, String city, int onFloor, boolean hasPool) {
        super(area, price, rooms, distToSchool, distToHospital, distToAirport, city, onFloor);
        this.hasPool = hasPool;
    }

    public boolean hasPool() {
        return hasPool;
    }

    public void setHasPool(boolean hasPool) {
        this.hasPool = hasPool;
    }

    @Override
    public String toString() {
        return "Penthouse{" +
                "id = " + getId() +
                ", area = " + getArea() +
                ", price = " + getPrice() +
                ", rooms = " + getRooms() +
                ", distance to school = " + getDistToSchool() +
                ", distance to hospital = " + getDistToHospital() +
                ", distance to airport = " + getDistToAirport() +
                ", city = '" + getCity() + '\'' +
                ", on floor = " + getOnFloor() +
                ", has pool=" + hasPool +
                '}';
    }
}
