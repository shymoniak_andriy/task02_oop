package com.shymoniak.model.realestates;

import java.util.concurrent.atomic.AtomicInteger;

public class RealEstate {
    //used to make an auto-increment id
    private static final AtomicInteger count = new AtomicInteger(0);
    private int id;
    private int area;
    private int price;
    private int rooms;
    private int distToSchool;
    private int distToHospital;
    private int distToAirport;
    private String city;

    public RealEstate() {
    }

    public RealEstate(int area, int price, int rooms, int distToSchool, int distToHospital,
                      int distToAirport, String city) {
        this.id = count.incrementAndGet();
        this.area = area;
        this.price = price;
        this.rooms = rooms;
        this.distToSchool = distToSchool;
        this.distToHospital = distToHospital;
        this.distToAirport = distToAirport;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    public int getDistToSchool() {
        return distToSchool;
    }

    public void setDistToSchool(int distToSchool) {
        this.distToSchool = distToSchool;
    }

    public int getDistToHospital() {
        return distToHospital;
    }

    public void setDistToHospital(int distToHospital) {
        this.distToHospital = distToHospital;
    }

    public int getDistToAirport() {
        return distToAirport;
    }

    public void setDistToAirport(int distToAirport) {
        this.distToAirport = distToAirport;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "RealEstate{" +
                "id = " + id +
                "area =  " + area +
                ", price = " + price +
                ", rooms = " + rooms +
                ", distToSchool = " + distToSchool +
                ", distToHospital = " + distToHospital +
                ", distToAirport = " + distToAirport +
                ", city = '" + city + '\'' +
                '}';
    }
}
