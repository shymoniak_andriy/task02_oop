package com.shymoniak.model.realestates;

public class Mansion extends RealEstate {
    private int storiesNumber;

    public Mansion() {
    }

    public Mansion(int area, int price, int rooms, int distToSchool, int distToHospital,
                   int distToAirport, String city, int storiesNumber) {
        super(area, price, rooms, distToSchool, distToHospital, distToAirport, city);
        this.storiesNumber = storiesNumber;
    }

    public int getStoriesNumber() {
        return storiesNumber;
    }

    public void setStoriesNumber(int storiesNumber) {
        this.storiesNumber = storiesNumber;
    }

    @Override
    public String toString() {
        return "Mansion{" +
                "id = " + getId() +
                ", area = " + getArea() +
                ", price = " + getPrice() +
                ", rooms = " + getRooms() +
                ", distance to school = " + getDistToSchool() +
                ", distance to hospital = " + getDistToHospital() +
                ", distance to airport = " + getDistToAirport() +
                ", city = '" + getCity() + '\'' +
                ", storiesNumber=" + storiesNumber +
                '}';
    }
}
