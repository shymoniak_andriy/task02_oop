package com.shymoniak.model;

import com.shymoniak.model.constants.ConsoleDefaultMessage;
import com.shymoniak.model.operations.BasicOperation;
import com.shymoniak.model.operations.SearchingOperation;
import com.shymoniak.model.realestates.Flat;
import com.shymoniak.model.realestates.Mansion;
import com.shymoniak.model.realestates.Penthouse;
import com.shymoniak.model.comparators.FlatComparator;
import com.shymoniak.model.comparators.MansionComparator;
import com.shymoniak.model.comparators.PenthouseComparator;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

/**
 * Uses methods from other model classes and combines them so that to
 * implement all functions of the program
 */
public class MainModel {
    BasicOperation basicOperation = new BasicOperation();
    SearchingOperation searchingOperation = new SearchingOperation();
    ConsoleDefaultMessage consoleDefaultMessage = new ConsoleDefaultMessage();

    /**
     * Prints all real estates in console
     * @param flats
     * @param mansions
     * @param penthouses
     */
    @SuppressWarnings("Duplicates")
    public void printAllRealEstates(ArrayList<Flat> flats, ArrayList <Mansion> mansions,
                                        ArrayList<Penthouse> penthouses) {
        for (Flat flat : flats) {
            System.out.println(flat);
        }
        for (Mansion mansion : mansions) {
            System.out.println(mansion);
        }
        for (Penthouse penthouse : penthouses) {
            System.out.println(penthouse);
        }
    }

    /**
     * These classes print specific real estate list in console
     * @param flats
     */
    public void printAllFlats(ArrayList<Flat> flats) {
        for (Flat flat : flats) {
            System.out.println(flat);
        }
    }

    public void printAllMansions(ArrayList<Mansion> mansions) {
        for (Mansion mansion : mansions) {
            System.out.println(mansion);
        }
    }

    public void printAllPenthouses(ArrayList<Penthouse> penthouses) {
        for (Penthouse penthouse : penthouses) {
            System.out.println(penthouse);
        }
    }

    /**
     * Finds and sorts real estates upon request
     *
     * @param fieldName the name of the field you want to change
     * @param tempStr  parameters for string fields
     * @param from      parameter for int lowest value
     * @param to        parameter for int highest value
     */
    @SuppressWarnings("Duplicates")
    public void findRealEstate(String fieldName, String tempStr, int from, int to,
                               ArrayList<Flat> flats, ArrayList <Mansion> mansions,
                               ArrayList<Penthouse> penthouses) {
        ArrayList<Flat> flatsFound = new ArrayList<>();
        ArrayList<Mansion> mansionsFound = new ArrayList<>();
        ArrayList<Penthouse> penthousesFound = new ArrayList<>();

        Set<Flat> flatSet = new TreeSet<>(new FlatComparator());
        Set<Mansion> mansionSet = new TreeSet<>(new MansionComparator());
        Set<Penthouse> penthouseSet = new TreeSet<>(new PenthouseComparator());

        if (fieldName.equals("price")) {
            flatsFound = searchingOperation.findFlatByPrice(from, to, flats);
            mansionsFound = searchingOperation.findMansionByPrice(from, to, mansions);
            penthousesFound = searchingOperation.findPenthouseByPrice(from, to, penthouses);
        } else if (fieldName.equals("city")) {
            flatsFound = searchingOperation.findFlatByCity(tempStr, flats);
            mansionsFound = searchingOperation.findMansionByCity(tempStr, mansions);
            penthousesFound = searchingOperation.findPenthouseByCity(tempStr, penthouses);
        } else if (fieldName.equals("area")) {
            flatsFound = searchingOperation.findFlatByArea(from, to, flats);
            mansionsFound = searchingOperation.findMansionByArea(from, to, mansions);
            penthousesFound = searchingOperation.findPenthouseByArea(from, to, penthouses);
        }

        for (Flat flat : flatsFound) {
            flatSet.add(flat);
        }
        for (Mansion mansion : mansionsFound) {
            mansionSet.add(mansion);
        }
        for (Penthouse penthouse : penthousesFound) {
            penthouseSet.add(penthouse);
        }

        for (Flat flat : flatSet) {
            System.out.println(flat.toString());
        }
        for (Mansion mansion : mansionSet) {
            System.out.println(mansion.toString());
        }
        for (Penthouse penthouse : penthouseSet) {
            System.out.println(penthouse.toString());
        }
    }

    /**
     * Deletes real estate by id, regardless of the type of real estate
     * @param id
     * @param flats
     * @param mansions
     * @param penthouses
     */
    @SuppressWarnings("Duplicates")
    public void deleteById(int id, ArrayList<Flat> flats, ArrayList <Mansion> mansions,
                            ArrayList<Penthouse> penthouses) {
        for (Flat flat : flats) {
            if (flat.getId() == id) {
                System.out.println("DELETING: " + flat.toString());
                basicOperation.deleteFlatById(id, flats);
                break;
            }
        }
        for (Mansion mansion : mansions) {
            if (mansion.getId() == id) {
                System.out.println("DELETING: " + mansion.toString());
                basicOperation.deleteMansionById(id, mansions);
                break;
            }
        }
        for (Penthouse penthouse : penthouses) {
            if (penthouse.getId() == id) {
                System.out.println("DELETING: " + penthouse.toString());
                basicOperation.deletePenthouseById(id, penthouses);
                break;
            }
        }
    }

    /**
     * Adds real estate to "Data Base"
     */
    public void addNewFlat(ArrayList<Flat> flats){
        Flat flat = consoleDefaultMessage.consoleFlatConstructor();
        basicOperation.addRealEstate(flat, flats);
    }

    public void addNewMansion(ArrayList<Mansion> mansions){
        Mansion mansion = consoleDefaultMessage.consoleMansionConstructor();
        basicOperation.addRealEstate(mansion, mansions);
    }

    public void addNewPenthouse(ArrayList<Penthouse> penthouses){
        Penthouse penthouse = consoleDefaultMessage.consolePenthouseConstructor();
        basicOperation.addRealEstate(penthouse, penthouses);
    }
}
