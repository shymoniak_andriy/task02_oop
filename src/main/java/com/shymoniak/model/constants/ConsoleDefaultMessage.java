package com.shymoniak.model.constants;

import com.shymoniak.model.realestates.Flat;
import com.shymoniak.model.realestates.Mansion;
import com.shymoniak.model.realestates.Penthouse;

import java.util.Scanner;

public class ConsoleDefaultMessage {
    public static void printMenu() {
        System.out.println("_________________________________________");
        System.out.println("|\t Choose an operation\t\t\t\t|");
        System.out.println("|\t\t\t\t\t\t\t\t\t\t|");
        System.out.println("|\t  Operations list:\t\t\t\t\t|\n" +
                "|\t1. Print all real estates\t\t\t|\n" +
                "|\t2. Print real estates by type\t\t|\n" +
                "|\t3. Find real estates by parameter\t|\n" +
                "|\t\t\t\t\t\t\t\t\t\t|\n" +
                "|\t4. Edit real estate\t\t\t\t\t|\n" +
                "|\t\t\t\t\t\t\t\t\t\t|\n" +
                "|\t9. Exit\t\t\t\t\t\t\t\t|");
        System.out.println("_________________________________________");
    }

    public void printRealEstateTypes() {
        System.out.println("Choose the type of real estate");
        System.out.println(" 1. flat");
        System.out.println(" 2. mansion");
        System.out.println(" 3. penthouse");
    }

    public void printSearchParameters() {
        System.out.println("Choose a parameter");
        System.out.println(" 1. price");
        System.out.println(" 2. city");
        System.out.println(" 3. area");
    }

    public void printRealEstateEditFunctions() {
        System.out.println("Choose an operation you want to perform");
        System.out.println(" 1. add new real estate");
        System.out.println(" 2. modify real estate");
        System.out.println(" 3. delete existing real estate");
    }

    Scanner scan = new Scanner(System.in);
    int tempInt;
    String tempStr;

    /**
     * Collects data entered by user and creates object from it
     * @return Flat, Mansion and Penthouse objects
     */
    @SuppressWarnings("Duplicates")
    public Flat consoleFlatConstructor() {
        Flat flat = new Flat();
        System.out.println("Please enter following fields:");
        System.out.println("area, price, rooms:");
        tempInt = scan.nextInt();
        flat.setPrice(tempInt);
        tempInt = scan.nextInt();
        flat.setArea(tempInt);
        tempInt = scan.nextInt();
        flat.setRooms(tempInt);
        System.out.println("distance to school, hospital, airport:");
        tempInt = scan.nextInt();
        flat.setDistToSchool(tempInt);
        tempInt = scan.nextInt();
        flat.setDistToHospital(tempInt);
        tempInt = scan.nextInt();
        flat.setDistToAirport(tempInt);
        System.out.println("the name of the city:");
        tempStr = scan.next();
        flat.setCity(tempStr);
        System.out.println("the number of the floor on which flat is located");
        tempInt = scan.nextInt();
        flat.setOnFloor(tempInt);

        System.out.println("NEW FLAT ADDED: " + flat.toString());
        return flat;
    }

    @SuppressWarnings("Duplicates")
    public Mansion consoleMansionConstructor() {
        Mansion mansion = new Mansion();
        System.out.println("Please enter following fields:");
        System.out.println("area, price, rooms:");
        tempInt = scan.nextInt();
        mansion.setPrice(tempInt);
        tempInt = scan.nextInt();
        mansion.setArea(tempInt);
        tempInt = scan.nextInt();
        mansion.setRooms(tempInt);
        System.out.println("distance to school, hospital, airport:");
        tempInt = scan.nextInt();
        mansion.setDistToSchool(tempInt);
        tempInt = scan.nextInt();
        mansion.setDistToHospital(tempInt);
        tempInt = scan.nextInt();
        mansion.setDistToAirport(tempInt);
        System.out.println("the name of the city:");
        tempStr = scan.next();
        mansion.setCity(tempStr);
        System.out.println("the number of stories in mansion");
        tempInt = scan.nextInt();
        mansion.setStoriesNumber(tempInt);

        System.out.println("NEW MANSION ADDED: " + mansion.toString());
        return mansion;
    }

    @SuppressWarnings("Duplicates")
    public Penthouse consolePenthouseConstructor() {
        Penthouse penthouse = new Penthouse();
        System.out.println("Please enter following fields:");
        System.out.println("area, price, rooms:");
        tempInt = scan.nextInt();
        penthouse.setPrice(tempInt);
        tempInt = scan.nextInt();
        penthouse.setArea(tempInt);
        tempInt = scan.nextInt();
        penthouse.setRooms(tempInt);
        System.out.println("distance to school, hospital, airport:");
        tempInt = scan.nextInt();
        penthouse.setDistToSchool(tempInt);
        tempInt = scan.nextInt();
        penthouse.setDistToHospital(tempInt);
        tempInt = scan.nextInt();
        penthouse.setDistToAirport(tempInt);
        System.out.println("the name of the city:");
        tempStr = scan.next();
        penthouse.setCity(tempStr);
        System.out.println("the number of the floor on which flat is located");
        tempInt = scan.nextInt();
        penthouse.setOnFloor(tempInt);
        System.out.println("is there a pool in the penthouse? (type 'yes' or 'no')");
        tempStr = scan.next();
        if (tempStr.equalsIgnoreCase("yes")){
            penthouse.setHasPool(true);
        } else if (tempStr.equalsIgnoreCase("no")){
            penthouse.setHasPool(false);
        } else {
            System.err.println("YOU ENTERED WRONG PARAMETER");
            System.err.println("PENTHOUSE WILL RECEIVE DEFAULT VALUE hasPool=false");
            penthouse.setHasPool(false);
        }

        System.out.println("NEW PENTHOUSE ADDED: " + penthouse.toString());
        return penthouse;
    }
}
