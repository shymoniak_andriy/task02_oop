package com.shymoniak.model.constants;

import java.util.ArrayList;
import com.shymoniak.model.realestates.Flat;
import com.shymoniak.model.realestates.Mansion;
import com.shymoniak.model.realestates.Penthouse;

/**
 * This class contains methods which fill "Data Base" with
 * initial information
 */
public class DefaultValue {

    public ArrayList<Flat> getFlatDefValues(){
        ArrayList<Flat> flats = new ArrayList<Flat>();

        flats.add(new Flat(80, 50000, 3, 5, 8, 15, "Lviv", 2));
        flats.add(new Flat(60, 45000, 2, 4, 2, 9, "Kyiv", 1));
        flats.add(new Flat(120, 85000, 5, 7, 8, 10, "Odessa", 5));
        flats.add(new Flat(90, 70000, 3, 3, 5, 12, "Lviv", 3));
        flats.add(new Flat(70, 50000, 3, 5, 10, 15, "Odessa", 7));
        flats.add(new Flat(130, 150000, 5, 9, 8, 20, "Lviv", 2));
        flats.add(new Flat(200, 240000, 6, 2, 3, 11, "Kyiv", 2));
        flats.add(new Flat(80, 50000, 3, 10, 7, 8, "Lviv", 2));
        flats.add(new Flat(75, 50000, 3, 10, 8, 9, "Kyiv", 2));
        flats.add(new Flat(50, 40000, 2, 9, 9, 7, "Odessa", 2));

        return flats;
    }

    public ArrayList<Mansion> getMansionDefValues(){
        ArrayList<Mansion> mansions = new ArrayList<Mansion>();
//
        mansions.add(new Mansion(250, 250000, 7, 8, 5, 15, "Lviv", 2));
        mansions.add(new Mansion(300, 350000, 8, 7, 8, 8, "Kyiv", 2));
        mansions.add(new Mansion(320, 350000, 9, 3, 7, 11, "Lviv", 2));
        mansions.add(new Mansion(280, 300000, 7, 12, 3, 9, "Odessa", 2));
        mansions.add(new Mansion(300, 300000, 8, 11, 1, 4, "Kyiv", 2));
        mansions.add(new Mansion(250, 240000, 7, 9, 2, 3, "Lviv", 2));
        mansions.add(new Mansion(200, 180000, 6, 1, 7, 17, "Lviv", 2));
        mansions.add(new Mansion(240, 210000, 7, 8, 4, 8, "Kyiv", 2));
        mansions.add(new Mansion(320, 340000, 9, 14, 6, 10, "Odessa", 2));
        mansions.add(new Mansion(270, 300000, 8, 9, 3, 12, "Lviv", 2));

        return mansions;
    }

    public ArrayList<Penthouse> getPenthouseDefValues(){
        ArrayList<Penthouse> penthouses = new ArrayList<Penthouse>();

        penthouses.add(new Penthouse(120, 200000, 4, 2, 4, 8, "Lviv", 9, true));
        penthouses.add(new Penthouse(180, 300000, 5, 2, 5, 9, "Kyiv", 12, true));
        penthouses.add(new Penthouse(200, 400000, 6, 1, 3, 7, "Kyiv", 10, true));
        penthouses.add(new Penthouse(150, 200000, 4, 3, 2, 10, "Odessa", 9, false));
        penthouses.add(new Penthouse(180, 270000, 5, 4, 1, 7, "Lviv", 9, true));
        penthouses.add(new Penthouse(210, 400000, 6, 4, 3, 6, "Lviv", 15, true));
        penthouses.add(new Penthouse(220, 450000, 6, 2, 2, 9, "Odessa", 12, true));
        penthouses.add(new Penthouse(120, 210000, 4, 3, 4, 5, "Odessa", 15, false));
        penthouses.add(new Penthouse(200, 380000, 5, 1, 4, 11, "Kyiv", 9, true));
        penthouses.add(new Penthouse(170, 280000, 5, 4, 5, 13, "Lviv", 11, true));
        penthouses.add(new Penthouse(110, 200000, 4, 2, 3, 8, "Lviv", 9, false));

        return penthouses;
    }
}
