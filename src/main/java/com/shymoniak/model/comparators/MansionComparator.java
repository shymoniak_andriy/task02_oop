package com.shymoniak.model.comparators;

import com.shymoniak.model.realestates.Mansion;

import java.util.Comparator;

/**
 * Compares mansions in order as follows:
 * price->area->rooms->distance to school->
 * distance to hospital->distance to airport->stories
 */
public class MansionComparator implements Comparator<Mansion> {
    @Override
    public int compare(Mansion o1, Mansion o2) {
        int res = Integer.compare(o1.getPrice(), o2.getPrice());
        if (res == 0) {
            res = Integer.compare(o1.getArea(), o2.getArea());
            if (res == 0) {
                res = Integer.compare(o1.getRooms(), o2.getRooms());
                if (res == 0) {
                    res = Integer.compare(o1.getDistToSchool(), o2.getDistToSchool());
                    if (res == 0) {
                        res = Integer.compare(o1.getDistToHospital(), o2.getDistToHospital());
                        if (res == 0) {
                            res = Integer.compare(o1.getDistToAirport(), o2.getDistToAirport());
                            if (res == 0) {
                                res = Integer.compare(o1.getStoriesNumber(), o2.getStoriesNumber());
                            }
                        }
                    }
                }
            }
        }
        return res;
    }
}
