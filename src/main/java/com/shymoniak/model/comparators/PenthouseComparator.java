package com.shymoniak.model.comparators;

import com.shymoniak.model.realestates.Penthouse;

import java.util.Comparator;

/**
 * Compares penthouses in order as follows:
 * price->area->rooms->distance to school->
 * distance to hospital->distance to airport->on floor->has pool
 */
public class PenthouseComparator implements Comparator<Penthouse> {
    @Override
    public int compare(Penthouse o1, Penthouse o2) {
        int res = Integer.compare(o1.getPrice(), o2.getPrice());
        if (res == 0) {
            res = Integer.compare(o1.getArea(), o2.getArea());
            if (res == 0) {
                res = Integer.compare(o1.getRooms(), o2.getRooms());
                if (res == 0) {
                    res = Integer.compare(o1.getDistToSchool(), o2.getDistToSchool());
                    if (res == 0) {
                        res = Integer.compare(o1.getDistToHospital(), o2.getDistToHospital());
                        if (res == 0) {
                            res = Integer.compare(o1.getDistToAirport(), o2.getDistToAirport());
                            if (res == 0) {
                                res = Integer.compare(o1.getOnFloor(), o2.getOnFloor());
                                if (res == 0) {
                                    res = Boolean.compare(o1.hasPool(), o2.hasPool());
                                }
                            }
                        }
                    }
                }
            }
        }
        return res;
    }
}
