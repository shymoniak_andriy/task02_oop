package com.shymoniak.model.comparators;

import com.shymoniak.model.realestates.Flat;

import java.util.Comparator;

/**
 * Compares flats in order as follows:
 * price->area->rooms->distance to school->
 * distance to hospital->distance to airport ->on floor
 */
public class FlatComparator implements Comparator<Flat> {
    @Override
    public int compare(Flat o1, Flat o2) {
        int res = Integer.compare(o1.getPrice(), o2.getPrice());
        if (res == 0) {
            res = Integer.compare(o1.getArea(), o2.getArea());
            if (res == 0) {
                res = Integer.compare(o1.getRooms(), o2.getRooms());
                if (res == 0) {
                    res = Integer.compare(o1.getDistToSchool(), o2.getDistToSchool());
                    if (res == 0) {
                        res = Integer.compare(o1.getDistToHospital(), o2.getDistToHospital());
                        if (res == 0) {
                            res = Integer.compare(o1.getDistToAirport(), o2.getDistToAirport());
                            if (res == 0) {
                                res = Integer.compare(o1.getOnFloor(), o2.getOnFloor());
                            }
                        }
                    }
                }
            }
        }
        return res;
    }
}
