package com.shymoniak.model.operations;

import com.shymoniak.model.realestates.Flat;
import com.shymoniak.model.realestates.Mansion;
import com.shymoniak.model.realestates.Penthouse;

import java.util.ArrayList;

/**
 * Adding, deleting and modifying methods are located here
 */
public class BasicOperation {
    /**
     * Overloaded methods for adding a new Real Estate
     */
    public void addRealEstate(Flat flat, ArrayList<Flat> flats) {
        flats.add(flat);
    }

    public void addRealEstate(Mansion mansion, ArrayList<Mansion> mansions) {
        mansions.add(mansion);
    }

    public void addRealEstate(Penthouse penthouse, ArrayList<Penthouse> penthouses) {
        penthouses.add(penthouse);
    }

    /**
     * Methods which allow to delete RealEstate from "DataBase" by ID
     */
    public void deleteFlatById(int id, ArrayList<Flat> flats) {
        for(int i = 0; i < flats.size(); i++){
            if(flats.get(i).getId() == id){
                flats.remove(flats.get(i));
                break;
            }
        }
    }

    public void deleteMansionById(int id, ArrayList<Mansion> mansions) {
        for(int i = 0; i < mansions.size(); i++){
            if(mansions.get(i).getId() == id){
                mansions.remove(mansions.get(i));
                break;
            }
        }
    }

    public void deletePenthouseById(int id, ArrayList<Penthouse> penthouses) {
        for(int i = 0; i < penthouses.size(); i++){
            if(penthouses.get(i).getId() == id){
                penthouses.remove(penthouses.get(i));
                break;
            }
        }
    }

    /**
     * Methods which allow to modify RealEstate in "DataBase" by ID
     */
    public void modifyFlatById(int id, Flat flat, ArrayList<Flat> flats) {
        flats.set(id, flat);
    }

    public void modifyMansionById(int id, Mansion mansion, ArrayList<Mansion> mansions) {
        mansions.set(id, mansion);
    }

    public void modifyPenthouseById(int id, Penthouse penthouse, ArrayList<Penthouse> penthouses) {
        penthouses.set(id, penthouse);
    }
}
