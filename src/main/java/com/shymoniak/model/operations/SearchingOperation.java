package com.shymoniak.model.operations;

import com.shymoniak.model.realestates.Flat;
import com.shymoniak.model.realestates.Mansion;
import com.shymoniak.model.realestates.Penthouse;

import java.util.ArrayList;

/**
 * Searching operations are located here
 */
public class SearchingOperation {

    /**
     * Searches real estates by price
     *
     * @return ArrayList filled with real estates sorted by price
     */
    public ArrayList<Flat> findFlatByPrice(int from, int to, ArrayList<Flat> flats) {
        ArrayList<Flat> res = new ArrayList<>();

        for (Flat flat : flats) {
            if ((flat.getPrice() >= from) && (flat.getPrice() <= to)) {
                res.add(flat);
            }
        }

        return res;
    }

    public ArrayList<Mansion> findMansionByPrice(int from, int to, ArrayList<Mansion> mansions) {
        ArrayList<Mansion> res = new ArrayList<>();

        for (Mansion mansion : mansions) {
            if ((mansion.getPrice() >= from) && (mansion.getPrice() <= to)) {
                res.add(mansion);
            }
        }

        return res;
    }

    public ArrayList<Penthouse> findPenthouseByPrice(int from, int to, ArrayList<Penthouse> penthouses) {
        ArrayList<Penthouse> res = new ArrayList<>();

        for (Penthouse penthouse : penthouses) {
            if ((penthouse.getPrice() >= from) && (penthouse.getPrice() <= to)) {
                res.add(penthouse);
            }
        }

        return res;
    }

    /**
     * Searches real estates by city name
     *
     * @return ArrayList filled with real estates sorted by city
     */
    public ArrayList<Flat> findFlatByCity(String cityName, ArrayList<Flat> flats) {
        ArrayList<Flat> res = new ArrayList<>();

        for (Flat flat : flats) {
            if (flat.getCity().equals(cityName)) {
                res.add(flat);
            }
        }

        return res;
    }

    public ArrayList<Mansion> findMansionByCity(String cityName, ArrayList<Mansion> mansions) {
        ArrayList<Mansion> res = new ArrayList<>();

        for (Mansion mansion : mansions) {
            if (mansion.getCity().equals(cityName)) {
                res.add(mansion);
            }
        }

        return res;
    }

    public ArrayList<Penthouse> findPenthouseByCity(String cityName, ArrayList<Penthouse> penthouses) {
        ArrayList<Penthouse> res = new ArrayList<>();

        for (Penthouse penthouse : penthouses) {
            if (penthouse.getCity().equals(cityName)) {
                res.add(penthouse);
            }
        }

        return res;
    }

    /**
     * Searches real estates by area
     *
     * @return ArrayList filled with real estates sorted by area
     */
    public ArrayList<Flat> findFlatByArea(int from, int to, ArrayList<Flat> flats) {
        ArrayList<Flat> res = new ArrayList<>();

        for (Flat flat : flats) {
            if ((flat.getArea() >= from) && (flat.getArea() <= to)) {
                res.add(flat);
            }
        }

        return res;
    }

    public ArrayList<Mansion> findMansionByArea(int from, int to, ArrayList<Mansion> mansions) {
        ArrayList<Mansion> res = new ArrayList<>();

        for (Mansion mansion : mansions) {
            if ((mansion.getArea() >= from) && (mansion.getArea() <= to)) {
                res.add(mansion);
            }
        }

        return res;
    }

    public ArrayList<Penthouse> findPenthouseByArea(int from, int to, ArrayList<Penthouse> penthouses) {
        ArrayList<Penthouse> res = new ArrayList<>();

        for (Penthouse penthouse : penthouses) {
            if ((penthouse.getArea() >= from) && (penthouse.getArea() <= to)) {
                res.add(penthouse);
            }
        }

        return res;
    }
}
