package com.shymoniak;

import com.shymoniak.view.UserInteraction;

public class Program {
    public static void main(String[] args) {
        UserInteraction userInteraction = new UserInteraction();
        userInteraction.consoleInterface();
    }
}
