package com.shymoniak.controller;

import com.shymoniak.model.constants.DefaultValue;
import com.shymoniak.model.MainModel;
import com.shymoniak.model.realestates.Flat;
import com.shymoniak.model.realestates.Mansion;
import com.shymoniak.model.realestates.Penthouse;

import java.util.ArrayList;

public class Controller {
    MainModel mainModel = new MainModel();
    DefaultValue defaultValue = new DefaultValue();

    ArrayList<Flat> flats = defaultValue.getFlatDefValues();
    ArrayList<Mansion> mansions = defaultValue.getMansionDefValues();
    ArrayList<Penthouse> penthouses = defaultValue.getPenthouseDefValues();

    public void printRealEstates() {
        mainModel.printAllRealEstates(flats, mansions, penthouses);
    }

    public void printFlats() {
        mainModel.printAllFlats(flats);
    }

    public void printMansions() {
        mainModel.printAllMansions(mansions);
    }

    public void printPenthouses() {
        mainModel.printAllPenthouses(penthouses);
    }

    public void searchByPrice(int from, int to) {
        mainModel.findRealEstate("price", "", from, to, flats, mansions, penthouses);
    }

    public void searchByArea(int from, int to) {
        mainModel.findRealEstate("area", "", from, to, flats, mansions, penthouses);
    }

    public void searchByCity(String city) {
        mainModel.findRealEstate("city", city, -1, -1, flats, mansions, penthouses);
    }

    public void deleteById(int id) {
        mainModel.deleteById(id, flats, mansions, penthouses);
    }

    public void addFlat() {
        mainModel.addNewFlat(flats);
    }

    public void addMansion() {
        mainModel.addNewMansion(mansions);
    }

    public void addPenthouse() {
        mainModel.addNewPenthouse(penthouses);
    }

    public void modifyFlat(int id){
        mainModel.deleteById(id, flats, mansions, penthouses);
        mainModel.addNewFlat(flats);
    }

    public void modifyMansion(int id){
        mainModel.deleteById(id, flats, mansions, penthouses);
        mainModel.addNewMansion(mansions);
    }

    public void modifyPenthouse(int id){
        mainModel.deleteById(id, flats, mansions, penthouses);
        mainModel.addNewPenthouse(penthouses);
    }

}
